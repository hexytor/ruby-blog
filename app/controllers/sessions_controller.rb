class SessionsController < ApplicationController
  skip_before_action :authorize , :only => [:new, :create]
  def new
  end
  def destroy
    user = User.find_by(name: params[:name])
    session[:user_id] = nil
    redirect_to '/'   
  end
  def create
    user = User.find_by(name: params[:name])
    if user and user.authenticate(params[:password])
      session[:user_id] = user.id
      session[:user_name] = user.name
      redirect_to user, alert: "کاربر وارد شد"
    else
      redirect_to login_url, alert: "نام کاربری یا گذر واژه صحیح نیست"
    end
  end
  # def destory
  #   User.find(session[:user_id]).destroy    
  #   session[:user_id] = nil         
  #   redirect_to '/' 
  # end
end
