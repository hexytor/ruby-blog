Rails.application.routes.draw do
  get 'admin/index'
  # get 'logout/destroy'
  get 'logout', to: 'sessions#destroy'
  #get 'sessions/create'
  # get 'sessions/destory'
  resources :users
  resources :posts
 


  get 'admin' => 'admin#index'
  # get 'logout' => 'sessions#destroy'

  controller :sessions do
    get  'login' => :new
    post 'login' => :create
    # delete 'logout' => :destroy
  end
  # get 'logout' => :destroy
  # delete 'logout' => :destroy
  root'admin#index'
end
